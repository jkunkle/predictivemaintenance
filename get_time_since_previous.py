import argparse
import os
import re
import datetime


def parse_args() : 

    parser = argparse.ArgumentParser( )

    parser.add_argument( '--fileName', dest='fileName', required=True, help='file to compare against' )
    parser.add_argument( '--dirPath', dest='dirPath', required=True, help='data directory to find files' )


    return parser.parse_args()


def main( args ) : 

    run ( **vars(args ) )

def run( fileName, dirPath ) : 

    check_datetime = get_timestamp_from_filename( fileName )
    found_check_file = False
    file_datetimes = []
    for f in os.listdir( dirPath ) : 

        if f == fileName : 
            found_check_file = True
            # don't save the datetime for this file to avoid confusion
            continue

        try : 
            file_datetimes.append( get_timestamp_from_filename( f ) )
        except AttributeError : 
            if f.count('.wav') : 
                print ('Could not parse file name, %s' %( f ) )
            continue
            


    if not found_check_file : 
        print('input file not found in directory!  Results may not make sense!')

    print (len( file_datetimes )) 
    earlier_times = list(filter( lambda x : x < check_datetime, file_datetimes ))
    diffs = min(list( map( lambda x : check_datetime - x, earlier_times )))
    print (diffs)

    


def get_timestamp_from_filename( fname )  : 

    res = re.match('recorded_audio_(\d{4})-(\d{2})-(\d{2})_(\d{2})-(\d{2})-(\d{2})\.wav', fname )

    int_groups = [int(x) for x in res.groups() ]

    return datetime.datetime( *int_groups )


if __name__ == '__main__' : 
    main( parse_args())


