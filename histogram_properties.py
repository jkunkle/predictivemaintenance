import matplotlib.pyplot as plt
import numpy as np
import os
import math
import wave
import json

import argparse

def parse_args()  : 

    parser = argparse.ArgumentParser( ) 

    parser.add_argument( '--dirpath', dest='dirpath', default=None, help = 'path to directory containing .wav files' ) 
    parser.add_argument( '--jsonfile', dest='jsonfile',  default=None, help= 'json file with stored data' ) 

    return parser.parse_args()


def main( args ) : 

    run( **vars( args ) )

def run( dirpath=None, jsonfile=None ) : 

    if dirpath is None and jsonfile is None : 
        raise RuntimeError( 'Must provide dirpath or jsonfile!')

    if dirpath is not None :

        max_vals = []
        count = 0
        for fname in os.listdir( dirpath ) : 
            #print (count)
            count += 1
            if not fname.count('.wav' ): 
                continue

            signal = read_wav( '%s/%s' %(dirpath, fname ) )

            #max_val = float(max( np.fabs(signal ) ) )
            #avg_val = float(np.mean( np.fabs(signal ) ) )
            #large_samples = len(list(filter( lambda x : math.fabs(x)>150, signal )))
            signal_fabs = np.fabs(signal)
            large_samples = len(signal_fabs[signal_fabs > 150])
            if large_samples > 5000 : 
                print ('%s : %d '%( fname, large_samples ) )
            # = float(max( np.fabs(signal ) ) )

            max_vals.append( large_samples )
            #max_vals.append( ( max_val, avg_val, large_samples) )
            #max_vals.append( ( max_val, avg_val) )


        ofile = open( 'histogram_data.json', 'w' )

        json.dump( max_vals , ofile )

        ofile.close()

    elif jsonfile is not None : 
        ofile = open( jsonfile ) 
        max_vals = json.load( ofile )
        ofile.close()

    #count_data = [x[1] for x in max_vals]
    count_data = max_vals
    print (count_data)

    good_data = list( filter( lambda x : x > 5000, count_data  ) ) 
    print (len(good_data)/len(count_data))

    bin_width= int(max(count_data)*1.1)//1000

    if bin_width == 0  : 
        bin_width = 1

    bins = range( 0, int(max(count_data)*1.1),  bin_width )


    plt.hist( count_data, bins )
    plt.yscale('log')
    plt.ylim( 0.1, 10000 )

    plt.show()

def read_wav( fname, dtype='Int16' ) : 

    try : 
        spf = wave.open(fname,'r')
        signal = spf.readframes(-1)
        signal = np.fromstring(signal, dtype)
        return signal


    except EOFError : 
        return []

    

if __name__ == '__main__' : 
    main( parse_args() )
