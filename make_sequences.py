import argparse
import json
import datetime
import os
import re
import sys
import numpy as np
import matplotlib.pyplot as plt
import pandas

_ALL_REGIONS = ['motor', 'grind', 'pump', 'clean']


from audio_region import audio_region
import utils

def parse_args() : 

    parser = argparse.ArgumentParser()
    
    parser.add_argument( '--audioDir', dest='audioDir', default=None, help='Path to directory containing input files' )
    parser.add_argument( '--dirsFile', dest='dirsFile', default=None, help='Path to file containing list of directories' )

    return parser.parse_args()


def main( args ) : 

    run( **vars( args ) )


def run( audioDir=None, dirsFile=None ) : 

    dirs_paths = []
    file_rois = []
    if audioDir is not None :
        dirs_paths.append( audioDir )
        file_rois.append( load_rois_from_path( audioDir )  )


    if dirsFile is not None : 
        with open( dirsFile ) as ofile : 
            for line in ofile : 
                d = line.rstrip('\n')
                file_rois.append( load_rois_from_path( d ) )
                dirs_paths.append( d)
                

    #rois = [item for sublist in file_rois for item in sublist]
    for frois, path in zip(file_rois,dirs_paths )  : 
        frois.sort( key=audio_region.get_start )

        frois = merge_adjacent_samples( frois )

        for f in frois : 
            print( f.get_metadata('source'))

        #make_grind_analytics( rois )
        #make_clean_analytics( rois )
        #make_poweroff_analytics( rois )
        make_sequences( frois, path )

        #make_analytics( rois ) 

def load_rois_from_path( path )  :

    try : 
        classification = load_classification_file( path ) 
    except FileNotFoundError: 
        print ('Failed to load classification file! ', path)
        raise
    try : 
        time_transfer = load_time_transfer_file( path ) 
    except FileNotFoundError: 
        print ('Failed to load time transfer file! ', path)
        raise

    time_offset = get_time_offset(time_transfer)

    return get_timestamped_rois( classification, time_offset )


def make_analytics( rois ) : 

    fig, axes = plt.subplots( 2, 2 ) 
    axes = axes.reshape( (4,))

    plot_types = _ALL_REGIONS

    for idx, typ in enumerate(plot_types) : 
        axes[idx].hist( [ utils.get_precise_seconds( x.get_length() ) for x in rois  if typ in x.get_value() ] )

    plt.show()

    

def get_timestamped_rois( classification, time_offset ) :

    timestamped_rois = []
    for fname, data in classification.items() : 

        try : 
            real_start_time = get_real_start_time( os.path.basename(fname), time_offset )
        except RuntimeError  : 
            print ('Failed to process file %s, continue' %( fname ))
            continue

        sample_rate = data['sample_rate']

        for roi in data['rois'] : 
            print (roi)

            roi_time = real_start_time + datetime.timedelta( seconds=roi['start']/sample_rate )

            this_roi = audio_region( roi['start'], 
                                     roi['stop'],
                                     roi['value'] )

            this_roi.convert_to_absolute_time( roi_time, sample_rate )
            this_roi.add_metadata('sample_rate', sample_rate )
            this_roi.add_metadata('source', fname )
            timestamped_rois.append( this_roi )


    return timestamped_rois

def merge_adjacent_samples( rois ) : 

    # check adjacent pairs of ROIs

    flag = 'cutoff'

    new_rois = []
    idx = -1
    n_cand = 0
    while( idx < len( rois) - 2 ) :
        idx += 1 
        roi1 = rois[idx]
        roi2 = rois[idx+1]

        if roi1.get_type() != 'absolute' or roi2.get_type() != 'absolute' : 
            raise RuntimeError ('ROIs must be in absolute time format!')

        merged = False
        new_labels = None
        if flag in roi1.get_value() and flag in roi2.get_value() : 
            n_cand += 1

            # check the time difference 
            time_stop1 = roi1.get_stop() 
            time_start2 = roi2.get_start()
            timediff = time_start2 - time_stop1

            if timediff < datetime.timedelta(seconds=3) : 
            
                # check labels
                labels1 = roi1 .get_value()
                labels2 = roi2.get_value() 

                if labels1 == labels2 :

                    new_labels = labels1
                    merged=True
                else : 
                    # there is a conflict in the labels
                    # probably one was only labeled as noise
                    # in this case keep both labels

                    # both have 'noise' and 'cutoff'
                    if len( labels1 ) + len( labels2) == 5 : 
                        new_labels = list( set( labels1 + labels2 )) 
                        merged=True
                    else : 
                        print ('Inconsistent labels!')
                        print (labels1)
                        print (labels2)

        if merged :

            
            md = dict(roi1.get_metadata())

            md['source'] = [roi1.get_metadata('source'), roi2.get_metadata('source')]
            md['sample_start'] = {roi1.get_metadata('source') : roi1.get_metadata('sample_start'), 
                                  roi2.get_metadata('source') : roi2.get_metadata('sample_start'), 
                                }
            md['sample_stop'] = {roi1.get_metadata('source') : roi1.get_metadata('sample_stop'), 
                                 roi2.get_metadata('source') : roi2.get_metadata('sample_stop'), 
                                }

            new_rois.append( audio_region( roi1.get_start(), roi2.get_stop(), new_labels, metadata=md ) )
            idx += 1
        else : 
            new_rois.append( roi1 )

    # add in the last ROI as well
    new_rois.append( rois[-1] )

    print (n_cand)
    return new_rois

def get_time_offset( transfer ) : 


    time_format = '%Y-%m-%d %H:%M:%S'
    time_real = datetime.datetime.strptime( transfer['real'], time_format )
    time_pi = datetime.datetime.strptime( transfer['pi'], time_format )

    return ( time_real - time_pi )

def get_real_start_time( fname, time_offset ) : 

    res = re.match( 'recorded_audio_(.*?)\.wav', fname )

    if res is None : 
        raise RuntimeError( 'Could not parse file name %s' %( fname ) )

    file_time = datetime.datetime.strptime(res.group( 1 ) , '%Y-%m-%d_%H-%M-%S' )

    return file_time + time_offset



        


def load_classification_file( data_dir ) : 

    ofile = open( '%s/classification.json' %data_dir, 'r' )

    data = json.load( ofile )

    ofile.close()

    return data

def load_time_transfer_file( data_dir ) : 

    ofile = open( '%s/time_transfer.json' %data_dir, 'r' )

    data = json.load( ofile )

    ofile.close()

    return data

    
def make_grind_analytics( sorted_rois ) : 

    motor_time_1_before_grind = []
    motor_time_2_before_grind = []
    motor_time_3_before_grind = []

    length_time_1_before_grind = []
    length_time_2_before_grind = []
    length_grind = []
    length_time_1_after_grind = []
    length_time_2_after_grind = []
    length_time_3_after_grind = []
    length_time_4_after_grind = []

    motor_time_1_after_grind = []
    pump_time_2_after_grind = []
    pump_time_3_after_grind = []

    motor_rois = []
    pump_rois = []

    for idx, roi in enumerate(sorted_rois) : 
        if 'motor' in roi.get_value( ) : 
            motor_rois.append( idx )
        if 'pump' in roi.get_value( ) : 
            pump_rois.append( idx )

    for idx, roi in enumerate(sorted_rois) : 

        if 'grind' in roi.get_value() : 

            length_grind.append( ( roi.get_length() ).total_seconds() )

            if (idx-1) in motor_rois : 
                diff = ( sorted_rois[idx-1].get_start() - roi.get_start() ).total_seconds()
                motor_time_1_before_grind.append( diff )
                if diff > -3 and diff < -2 : 
                    length_time_1_before_grind.append( (sorted_rois[idx-1].get_length()).total_seconds())
            if (idx-2) in motor_rois :
                diff = ( sorted_rois[idx-2].get_start() - roi.get_start()  ).total_seconds()
                motor_time_2_before_grind.append( diff )
                if diff > -6 and diff < -5 : 
                    length_time_2_before_grind.append( (sorted_rois[idx-2].get_length()).total_seconds())
            if (idx-3) in motor_rois :
                motor_time_3_before_grind.append( ( roi.get_start() - sorted_rois[idx-3].get_start() ).total_seconds() )

            if (idx+1) in motor_rois : 
                diff = ( sorted_rois[idx+1].get_start() - roi.get_stop() ).total_seconds() 
                motor_time_1_after_grind.append( diff )
                if diff < 10 : 
                    length_time_1_after_grind.append( (sorted_rois[idx+1].get_length()).total_seconds())
            if (idx+2) in pump_rois : 
                diff = ( sorted_rois[idx+2].get_start() - roi.get_stop() ).total_seconds() 
                pump_time_2_after_grind.append( diff )
            if (idx+3) in pump_rois : 
                pump_time_3_after_grind.append( ( sorted_rois[idx+3].get_start() - roi.get_stop() ).total_seconds() )
            if (idx+4) in motor_rois : 
                length_time_4_after_grind.append( sorted_rois[idx+4].get_length().total_seconds() )
                




    fig, axes = plt.subplots( 1, 4 ) 

    bins_1 = get_bins_range( motor_time_1_before_grind, 100 )
    bins_2 = get_bins_range( motor_time_2_before_grind, 100 )
    bins_3 = get_bins_range( motor_time_3_before_grind, 100 )

    axes[0].hist( motor_time_1_before_grind, bins = np.linspace(-20, 20, 100) )
    axes[1].hist( motor_time_2_before_grind, bins = np.linspace(-20, 20, 100) )
    axes[2].hist( motor_time_3_before_grind, bins = np.linspace(0, 190, 100) )
    axes[3].hist( motor_time_1_after_grind, bins = np.linspace(0, 10, 100) )

    plt.show()

    fig, axes = plt.subplots( 1, 5 ) 
    axes[0].hist( length_time_2_before_grind, bins = np.linspace( 0, 10, 100 ) )
    axes[1].hist( length_time_1_before_grind, bins = np.linspace( 0, 10, 100 ) )
    axes[2].hist( length_grind, bins = np.linspace( 0, 10, 100 ) )
    axes[3].hist( length_time_1_after_grind, bins = np.linspace( 0, 10, 100 ) )
    axes[4].hist( length_time_4_after_grind, bins = np.linspace( 0, 10, 100 ) )
    plt.show()


    fig, axes = plt.subplots( 1, 2 ) 
    axes[0].hist( pump_time_2_after_grind, bins = np.linspace(0, 20, 100) )
    axes[1].hist( pump_time_3_after_grind, bins = np.linspace(0, 30, 100) )
    plt.show()

def make_clean_analytics( sorted_rois ) : 

    motor_time_1_before_clean = []
    motor_time_2_before_clean = []
    motor_time_1_after_clean = []

    motor_rois = []

    for idx, roi in enumerate(sorted_rois) : 
        if 'motor' in roi.get_value( ) : 
            motor_rois.append( idx )

    for idx, roi in enumerate(sorted_rois) : 

        if 'clean' in roi.get_value() : 

            if (idx-1) in motor_rois : 
                motor_time_1_before_clean.append( ( sorted_rois[idx-1].get_start() - roi.get_start() ).total_seconds() )
            if (idx-2) in motor_rois :
                motor_time_2_before_clean.append( (  sorted_rois[idx-2].get_start() - roi.get_start()).total_seconds() )

            if (idx+1) in motor_rois :
                motor_time_1_after_clean.append(  ( sorted_rois[idx+1].get_start() - roi.get_stop()  ).total_seconds() )


    fig, axes = plt.subplots( 1, 3 ) 

    axes[0].hist( motor_time_1_before_clean, bins = np.linspace(-20, 0, 100) )
    axes[1].hist( motor_time_2_before_clean, bins = np.linspace(-50, 0, 100) )
    axes[2].hist( motor_time_1_after_clean, bins = np.linspace(0, 20, 100) )

    plt.show()

def make_poweroff_analytics( sorted_rois ) :

    motor_time_1_before_off = []
    motor_time_1_after_off = []

    motor_rois = []
    for idx, roi in enumerate(sorted_rois) : 
        if 'motor' in roi.get_value( ) : 
            motor_rois.append( idx )

    for idx, roi in enumerate(sorted_rois) : 

        if 'pump' in roi.get_value() : 

            if (idx-1) in motor_rois : 
                motor_time_1_before_off.append( ( sorted_rois[idx-1].get_start() - roi.get_start() ).total_seconds() )
            if (idx+1) in motor_rois :
                motor_time_1_after_off.append(  ( sorted_rois[idx+1].get_start() - roi.get_stop()  ).total_seconds() )


    fig, axes = plt.subplots( 1, 3 ) 

    axes[0].hist( motor_time_1_before_off, bins = np.linspace(-20, 0, 100) )
    axes[1].hist( motor_time_1_after_off, bins = np.linspace(0, 20, 100) )

    plt.show()

def get_bins_range( vals, nbins ) : 

    min_val = min( vals )
    max_val = max( vals ) 

    bins = np.linspace( min_val*0.9, max_val*1.1, nbins )

    return bins


def make_sequences( sorted_rois, output_dir=None ) : 

    #matches += ['motor', 'pump', 'motor']
    #matches += ['motor', 'clean', 'motor']

    remaining_rois, grind_sequences = filter_grind_sequences( sorted_rois )

    remaining_rois, clean_sequences = filter_clean_sequences( remaining_rois )

    remaining_rois, poweroff_sequences = filter_poweroff_sequences( remaining_rois )


    #print ('REMAIN')
    #for r in remaining_rois : 
    #    print (r)
    for r in grind_sequences : 
        print ('NEW SEQUENCE')
        for _r in r : 
            print (_r)
            print (_r.get_metadata())

    #plot_coffees_per_day( grind_sequences )

    if output_dir is not None : 

        data = []
        for r in grind_sequences + clean_sequences + poweroff_sequences : 
            for _r in r :
                data.append( _r.to_dict() )

        for i, item in enumerate(data) : 
            for mkey, mval in item['metadata'].items() : 
                data[i][mkey] = mval
            data[i].pop( 'metadata')

        df = pandas.DataFrame.from_dict( data )
        print (df)

        df.to_csv( '%s/sequences.csv' %output_dir )

        #out_path = '%s/sequences.json' %output_dir

        #ofile = open( out_path, 'w' )
        #json.dump( data, ofile, default=str )

        #ofile.close()



def filter_clean_sequences( sorted_rois ) :

    pattern = [ 
         
        { 'type' : 'motor', 'length' : (1, 6), 'time_since_anchor' : {'limit' : ( -60, -5 ), 'where' : ('start','start')},'required' : True},  
        { 'type' : 'motor', 'length' : (1, 6), 'time_since_anchor' : {'limit' : ( -25, -1 ), 'where' : ('start','start')}, 'required' : True },  
        { 'type' : 'clean', 'length' : (30, 40),                                                                         'required' : True, 'anchor':True },  
        { 'type' : 'motor', 'length' : (1, 6), 'time_since_anchor' : {'limit' : ( 0, 3 ), 'where' : ('stop','start')}, 'required' : True },  
    ]

    return find_pattern_matches( sorted_rois, pattern, 'clean' ) 

def filter_poweroff_sequences( sorted_rois ) :

    pattern = [ 
         
        { 'type' : 'motor', 'length' : (2, 7), 'time_since_anchor' : {'limit' : ( -10, 0 ), 'where' : ('start','start')},'required' : True},  
        { 'type' : 'pump', 'length' : (9, 16),                                                                         'required' : True, 'anchor':True },  
        { 'type' : 'motor', 'length' : (2, 7), 'time_since_anchor' : {'limit' : ( 0, 8 ), 'where' : ('stop','start')}, 'required' : True },  
    ]

    return find_pattern_matches( sorted_rois, pattern, 'poweroff' ) 


def filter_grind_sequences( sorted_rois ) : 

    pattern = [ 
        { 'type' : 'motor', 'length' : (1, 3), 'time_since_anchor' : {'limit' : ( -6, -4 ), 'where' : ('start','start')},'required' : False },  
        { 'type' : 'motor', 'length' : (1, 3), 'time_since_anchor' : {'limit' : ( -3, -1 ), 'where' : ('start','start')}, 'required' : False },  
        { 'type' : 'grind', 'length' : (1, 15),                                                                         'required' : True, 'anchor':True },  
        { 'type' : 'motor', 'length' : (3, 6), 'time_since_anchor' : {'limit' : ( 0, 6 ), 'where' : ('stop','start')},'required' : True },  
        { 'type' : 'pump' , 'length' : (3, 6), 'time_since_anchor' : {'limit' : ( 7, 13 ), 'where' : ('stop','start')},'required' : True },  
        { 'type' : 'pump' ,                    'time_since_anchor' : {'limit' : ( 15, 21 ), 'where' : ('stop','start')},'required' : True },  
        { 'type' : 'motor', 'length' : (2, 7), 'required' : True },  
    ]


    return find_pattern_matches( sorted_rois, pattern, 'grind' ) 

def find_pattern_matches( rois, pattern, label=None ) : 

    anchor_offset = 0
    for pidx, pat in enumerate(pattern) : 
        if pat.get('anchor', False ) : 
            anchor_offset = pidx
            break

    required_pat_idx = []
    for pidx, pat in enumerate(pattern) : 
        if pat.get('required', False ) : 
            required_pat_idx.append( pidx )

    pattern_matches = []
    used_idx = []
    for idx, roi in enumerate( rois ) : 

        this_seq_rois = []
        pat_match_idx = []
        seq_match_idx = []
        for pidx, pat in enumerate(pattern) : 

            #try : 
            #    print ('ON ROI')
            #    print (rois[pidx+idx])
            #except : 
            #    pass

            #print ('Anchor idx = %d, pattern_idx = %d, idx = %d'% ( idx+anchor_offset-pidx, pidx, idx+pidx ) )
            if idx+pidx in used_idx : 
                #print ('Skipping used roi')
                continue
            if matches_expectation( rois, idx+anchor_offset, idx+pidx, pat ) : 
                this_seq_rois.append( rois[idx+pidx] )
                this_seq_rois[-1].add_metadata( 'AssignedType', pat['type'] )
                if label is not None : 
                    this_seq_rois[-1].add_metadata( 'SequenceLabel', label )
                pat_match_idx.append( pidx )
                seq_match_idx.append( idx+pidx )
            else : 
                # if we missed a required pattern, then exit
                if pat.get('required', False ) : 
                    #print ('FAIL ReQ')
                    this_seq_rois =[]
                    pat_match_idx = []
                    seq_match_idx = []
                    break

        if set( pat_match_idx ).issuperset( required_pat_idx ) : 
            pattern_matches.append( this_seq_rois )
            used_idx += seq_match_idx
            this_seq_rois = []
            pat_match_idx = []
            seq_match_idx = []

    remain_rois = []
    for seq in rois : 
        found = False
        for _s in pattern_matches: 
            if seq in _s : 
                found = True
                break
        if not found : 
            remain_rois.append( seq)

    return remain_rois, pattern_matches


def matches_expectation( all_rois, idx_anchor, idx_compare, params ) : 

    #print (params)

    if idx_anchor < 0 or idx_anchor >= len(all_rois) : 
        print ('inDEX out of range')
        return False

    if idx_compare < 0 or idx_compare >= len(all_rois) : 
        print ('inDEX out of range')
        return False

    anchor_region = all_rois[idx_anchor]
    comp_region = all_rois[idx_compare]

    pass_diff = False
    anchor_params = params.get( 'time_since_anchor', None )

    if anchor_params is not None : 
        if anchor_params['where'][0] == 'start' : 
            anchor_time = anchor_region.get_start()
        if anchor_params['where'][0] == 'stop' : 
            anchor_time = anchor_region.get_stop()
    
        if anchor_params['where'][1] == 'start' : 
            comp_time = comp_region.get_start()
        if anchor_params['where'][1] == 'stop' : 
            comp_time = comp_region.get_stop()

        diff = (comp_time - anchor_time).total_seconds()
        #print ('diff = ', diff )

        if diff > anchor_params['limit'][0] and diff < anchor_params['limit'][1] : 
            pass_diff = True
    else : 
         pass_diff = True

    
    pass_length = False
    if 'length' in params : 

        reg_len = ( comp_region.get_length() ).total_seconds()
        #print ('len = ', reg_len )

        if reg_len > params['length'][0] and reg_len < params['length'][1] : 
            pass_length = True
    else : 
        pass_length = True

    if 'cutoff' in comp_region.get_value() : 
        pass_length = True

    value = params['type']
    #print ('val = ', comp_region.get_value())
    pass_value = False
    if value in comp_region.get_value() or not set( comp_region.get_value() ) .intersection( set(_ALL_REGIONS ) ) :
        pass_value = True

    #print ('pass_diff ', pass_diff )
    #print ('pass_length', pass_length)
    #print ('pass_value', pass_value)
    return pass_diff & pass_length & pass_value


def plot_coffees_per_day( sequences ) : 

    coffee_times = [ s[0].get_start() for s in sequences ]

    df = pandas.DataFrame( {'date-hour' : [t.replace(minute=0, second=0, microsecond=0) for t in coffee_times ] } )
    #df = pandas.DataFrame( {'date-hour' : [t.date() for t in coffee_times ] } )
    df['count'] = 1

    res = df.groupby( 'date-hour', as_index=False).sum()

    times = list(res['date-hour'])
    counts = res['count']

    time_min = times[0]
    time_max = times[len(times)-1]

    thistime = time_min
    add_times = []
    while thistime < time_max : 

        thistime = thistime + datetime.timedelta(hours=1)

        if thistime not in times : 
            add_times.append(( thistime, 0 ))

    all_times =[ (x,y) for x, y in zip( times, counts ) ]
    all_times += add_times
    all_times.sort()


    plt.plot(*zip(*all_times))
    plt.show()
    


if __name__ == '__main__' : 
    main( parse_args() )
