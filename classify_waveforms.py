import argparse
import pickle
import os
import json


import make_cluster_model
import plot_cluster_result
from audio_region import audio_region
import utils

def parse_args() : 

    parser = argparse.ArgumentParser()
    
    parser.add_argument( '--audioFile', dest='audioFile', default=None, help='Path to input file' )
    parser.add_argument( '--audioDir', dest='audioDir', default=None, help='Path to directory containing input files' )
    parser.add_argument( '--clusterModelFile', dest='clusterModelFile', required=True,  help='Path to pickle file with cluster parameters data' )
    parser.add_argument( '--clusterMappingFile', dest='clusterMappingFile', default=None, help='JSON file containing the mapping of cluster index to string' )

    parser.add_argument( '--clusterModelFileFine', dest='clusterModelFileFine', default=None,  help='Path to pickle file with cluster parameters data' )
    parser.add_argument( '--clusterMappingFileFine', dest='clusterMappingFileFine', default=None, help='JSON file containing the mapping of cluster index to string' )
    parser.add_argument( '--visualize', dest='visualize', default=False, action='store_true', help='Show graphs for each step of the process' )


    return parser.parse_args()


def main( args ) : 

    run( **vars( args ) )

def run( audioFile, audioDir, clusterModelFile, clusterModelFileFine=None, clusterMappingFile=None, clusterMappingFileFine=None, visualize=False ) : 

    classification_results = { } 

    model = utils.get_model( clusterModelFile )
    model_fine = utils.get_model( clusterModelFileFine )

    input_files = []
    if audioDir is not None : 
        for fname in os.listdir( audioDir ) : 
            if fname.count('.wav') : 
                input_files.append( '%s/%s' %( audioDir, fname ) )

    if audioFile is not None : 
        input_files.append( audioFile )

    for fname in input_files : 

        try : 
            sample_rate, samples = make_cluster_model.read_file( fname )
        except ValueError as e : 
            print (e)
            continue

        # use a higher threshold for longer samples
        if samples.shape[0] < 5000000 : 
            thresholds = ( 0.9, 0.9 ) 
        else : 
            thresholds = ( 0.95, 0.97 ) 

        prediction, scale = get_predictions( samples, sample_rate, model )

        category_ranges, total_range, selected_range = categorize_sample( prediction, scale, model.FPS, model.mapping ) 

        noise_mapping = {}
        for key, val in model.mapping.items() : 
            if val != 'silence' : 
                noise_mapping[key] = 'noise'
            else : 
                noise_mapping[key] = val

        category_ranges_noise = categorize_sample_noise( prediction, scale, model.FPS, noise_mapping ) 


        frac = selected_range / total_range

        print (fname)
        print ('Selected %f of the range'%frac )

        n_clean_regions = 0

        if frac < thresholds[0] : 
            # check the file for periodicity

            # get the spectrogram for prediction
            fine_spectro, spectro_scale = make_cluster_model.make_coarse_spectrogram( samples, sample_rate, target_fps=model_fine.FPS )

            # run the prediction
            predicted_clusters = model_fine.predict( fine_spectro )

            # convert the predictions into ranges in sample space
            prediction_ranges = get_unmerged_predictions( predicted_clusters, spectro_scale )

            # map the groups to names
            convert_to_group_names( prediction_ranges, model_fine.mapping )

            pattern = [ ['silence', 'noise'], ['silence', 'noise', 'pump'], ['silence', 'noise', 'pump'], ['pump'], ['pump'], ['pump'], ['pump', 'silence', 'noise'], ['pump', 'silence', 'noise'], ['silence', 'noise'] ]
            max_wild = 1
            additional_ranges = find_pattern_matching_regions( prediction_ranges, spectro_scale, pattern, max_wild )

            category_ranges , new_selected = merge_ranges_overlap_removed( category_ranges, additional_ranges, selected_range )

            n_clean_regions = len( additional_ranges )

            frac =  new_selected/total_range
            print ('New fraction = %f' %(frac))

        rois = label_regions_of_interest( category_ranges_noise, category_ranges ) 
        print ('ROIS')
        for r in rois : 
            print (r)
        # fill mapped ranges 

        classification_results[fname] = {'rois' : [], 'quality' : frac, 'n_clean_regions' : n_clean_regions, 'sample_rate' : sample_rate }

        for roi in rois : 
            is_cutoff = False
            samples_len = samples.shape[0]
            if int(roi.get_stop()) == samples_len : 
                is_cutoff = True
            if int(roi.get_start() ) == 0 : 
                is_cutoff = True
            if is_cutoff :
                roi.set_value( roi.get_value() + ['is_cutoff'] )
            else :
                roi.set_value( roi.get_value() )

        for roi in rois : 
            this_data = roi.to_dict()

            classification_results[fname]['rois'].append( this_data )

        if visualize : 
            plot_cluster_result.plot_samples_with_ranges( samples, [category_ranges_noise, category_ranges])

    total_files = 0
    fail_files = []
    for fname, fdata in classification_results.items() : 
        total_files += 1

        if fdata['n_clean_regions'] > 4 or fdata['quality'] < 0.95 : 
            fail_files.append( fname )

    print ('Had %d poor categorizations out of %d' %( len(fail_files), total_files ))
    for f in fail_files : 
        print ('File : %s, quality : %f, foudn n_clean = %d' %( f, classification_results[f]['quality'], classification_results[f]['n_clean_regions']))

    if audioDir is not None : 

        ofile = open( '%s/classification.json' %audioDir, 'w' ) 

        json.dump( classification_results, ofile )

        ofile.close()



def get_predictions( samples, sample_rate, model ) : 

    coarse_spectro, spectro_scale = make_cluster_model.make_coarse_spectrogram( samples, sample_rate, target_fps=model.FPS )

    predicted_clusters = model.predict( coarse_spectro )

    return predicted_clusters, spectro_scale

def merge_ranges_overlap_removed( original_ranges, overlap_ranges, selected_range ) :

    # now remove any original ranges that overlap with this
    new_selected = selected_range
    merged_ranges = []

    # first look at all overlaps and
    # deal with partial overlaps by 
    # checking edges
    # keep a new list of ranges so that
    # we can adjust overlap boundaries
    #mod_overlap_ranges = [] 
    #for ostart, ostop, oval in overlap_ranges : 
    #    new_ostart = ostart
    #    new_ostop  = ostop
    #    for start, stop, val in original_ranges : 
    #        if start < ostart and stop > ostart : 
    #            new_ostart = stop
    #        if start < ostop and stop > ostop : 
    #            new_ostop = start

    #    mod_overlap_ranges.append( ( new_ostart, new_ostop, oval ) )

    #for start, stop, val in original_ranges : 
    #    overlaps_added = False
    #    for astart, astop, aval in mod_overlap_ranges : 
    #        if start >= astart and stop <= astop : 
    #            overlaps_added = True
    #    if overlaps_added : 
    #        new_selected -= ( stop - start )
    #    else : 
    #        merged_ranges.append( (start, stop, val ) )

    for orig_range in original_ranges : 
        orig_start = orig_range.get_start()
        orig_stop  = orig_range.get_stop()
        orig_val   = orig_range.get_value()
        overlaps_added = False
        for olap_range in overlap_ranges : 
            olap_start = olap_range.get_start()
            olap_stop  = olap_range.get_stop()
            olap_val   = olap_range.get_value()
            # overlap fully covers don't keep the original
            if orig_start >= olap_start and orig_stop <= olap_stop : 
                new_selected -= ( orig_stop - orig_start )
                overlaps_added = True
            # overlap is fully covered
            # split the original into a before and after
            elif orig_start <= olap_start and orig_stop >= olap_stop : 
                new_len = olap_start - orig_start
                if new_len > 0 :
                    merged_ranges.append( audio_region(orig_start, olap_start, orig_val) )
                    overlaps_added = True
                new_len = orig_stop - olap_stop
                if new_len > 0 :
                    merged_ranges.append( audio_region(olap_stop, orig_stop, orig_val) )
                    overlaps_added = True

                # remvoe the overlap region from the selected
                # count because it will be added later
                new_selected -= ( olap_range.get_length() )

        if not overlaps_added : 
            merged_ranges.append( orig_range )

    mod_overlap_ranges = [] 
    for olap_range in overlap_ranges : 
        new_ostart = olap_range.get_start()
        new_ostop  = olap_range.get_stop()
        for orig_range in original_ranges : 
            # partial overlap at beginning
            if olap_range.get_begin_overlap_fraction( orig_range ) > 0 : 
                new_ostart = orig_range.get_stop()
            # partial overlap at end
            if olap_range.get_end_overlap_fraction( orig_range ) > 0 : 
                new_ostop = orig_range.get_start()

        if new_ostart == new_ostop : 
            continue
        mod_overlap_ranges.append( audio_region( new_ostart, new_ostop, olap_range.get_value()) )

    for ar in mod_overlap_ranges : 
        new_selected += ( ar.get_length() )
        merged_ranges.append (ar )
    
    
    return( merged_ranges, new_selected)



def find_pattern_matching_regions( mapped_ranges, scale,  pattern, max_wild ) :

    matches = find_sliding_window_matches( mapped_ranges, pattern, max_wild ) 

    # now group them and add into ranges
    range_start = None
    range_end = None

    added_ranges = []
    for mstart, mend in matches : 
        if range_start is None : 
            range_start = mstart
            range_end = mend
        else : 
            diff = mstart - range_end
            if diff > 9*3*scale : 
                added_ranges.append( audio_region(range_start, range_end, 'clean' ) )
                print ('Add category')
                print (added_ranges[-1] )
                print ('diff = %f, limit = %f' %( diff, 9*3*scale ) )
                range_start = mstart
                range_end = mend
            # otherwise update the end
            else : 
                range_end = mend

    if range_start is not None : 
        added_ranges.append( audio_region( range_start, range_end, 'clean' ) )
        print ('Add category')
        print (added_ranges[-1] )


    added_ranges = list(filter( lambda x :  x.get_length() > scale*10, added_ranges ))

    return added_ranges

def convert_to_group_names( ranges, mapping ) : 

    mapped_ranges = []
    for rang in ranges : 
        try : 
            rang.set_value( mapping[rang.get_value()])
        except KeyError : 
            rang.set_value( 'unknown' )

        mapped_ranges.append( rang )

    return mapped_ranges

def find_sliding_window_matches( ranges, pattern, max_wild ) : 

    # find all valid values so that we can count wild cards
    all_valid = []
    for x in pattern : 
        all_valid += x
    

    all_valid = list( set( all_valid ) )
    pattern_matches = []

    for istart in range( 0, len(ranges) ) : 
        matches = 0
        used_wild = 0

        for ipatt, patt_vals in enumerate( pattern ) : 
            icheck = istart + ipatt
            if icheck >= len( ranges ) : 
                break
            check_val = ranges[icheck].get_value()
            if check_val not in all_valid : 
                matches += 1
                used_wild += 1
            else : 
                if check_val in patt_vals : 
                    matches += 1

        if matches == len( pattern ) and used_wild <= max_wild : 
            start = ranges[istart].get_start()
            endidx = istart + len( pattern) - 1
            if endidx >= len(ranges) : 
                endidx = len(ranges)-1
            stop = ranges[endidx].get_stop()
            
            pattern_matches.append( ( start, stop) )

        #print ('%d : %d' %( matches, used_wild))

    return pattern_matches

def categorize_sample_noise( predictions, spectro_scale, fps, mapping ) : 

    prediction_ranges = get_merged_predictions_simple( predictions, spectro_scale )

    # convert now so that the mapping can map multiple groups to the same value
    convert_to_group_names( prediction_ranges, mapping )

    merged_ranges = merge_ranges_liberal( prediction_ranges, spectro_scale)

    return merged_ranges

def categorize_sample( predictions, spectro_scale, fps, mapping) : 

    prediction_ranges = get_merged_predictions_simple( predictions, spectro_scale )

    cutoff = 64//(196//fps)

    merged_ranges = merge_ranges_conservative( prediction_ranges, spectro_scale, cutoff )

    RANGE_THRESHOLD = 160//(196//fps)

    final_ranges, total_range, selected_range = remove_small_ranges( merged_ranges, RANGE_THRESHOLD*spectro_scale ) 

    convert_to_group_names( final_ranges, mapping )

    return final_ranges, total_range, selected_range
    

def remove_small_ranges( merged_ranges, threshold, exempt=None ) : 

    if exempt is None : 
        exempt = []

    if not isinstance( exempt, list ) : 
        exempt = [exempt]

    filtered_ranges = []
    selected_range = 0
    total_range = 0
    for rang in merged_ranges : 
        length = rang.get_length()
        total_range += length
        if length > threshold or rang.get_value() in exempt : 
            selected_range += length
            filtered_ranges.append( rang ) 

    return filtered_ranges, total_range, selected_range
    

def merge_ranges_conservative( prediction_ranges, scale, cutoff=8 ) : 

    merged_ranges = []
    # must manually loop so that we have control of the index
    ridx = -1
    group_before_merge = None
    collected_to_merge_start = None
    collected_to_merge = []
    collected_to_merge_length = 0
    while ridx < (len(prediction_ranges)-1) : 
        ridx += 1

        this_range = prediction_ranges[ridx]
        length = this_range.get_length()
        start  = this_range.get_start()
        stop   = this_range.get_stop()
        val    = this_range.get_value()


        merge_length = length + collected_to_merge_length

        if merge_length < scale*cutoff: 
            # this is a candidate for merging

            # set this as the first candidate if one not set
            if collected_to_merge_start is None : 
                collected_to_merge_start = start

                # handle the case of the first index
                if ridx != 0 : 
                    group_before_merge = prediction_ranges[ridx-1].get_value()

            collected_to_merge_length += length
            collected_to_merge.append( this_range )    

            
        else : 
            if collected_to_merge_start is not None : 
                if collected_to_merge_length < scale*cutoff and ( group_before_merge is None or (val == group_before_merge) ) : 
                    # merge! add a new sample with the start of the first collected
                    merged_ranges.append( audio_region( collected_to_merge_start, stop, val ) )

                else : 
                    # we falied to merge some ranges, go through and fill them
                    # along with the current range
                    collected_to_merge.append( this_range )
                    for tup in collected_to_merge : 
                        merged_ranges.append( tup )


                #  reset ( whether merged or not)
                collected_to_merge_length = 0
                collected_to_merge_start = None
                group_before_merge = None
                collected_to_merge = []
            else : 
                # no merge available, just add self
                merged_ranges.append( this_range )
                    

    fixed_merged = merge_adjacent_groups( merged_ranges ) 

    # do it again until there are no more changes
    if len( fixed_merged ) < len( prediction_ranges ) : 
        return merge_ranges_conservative( fixed_merged, scale, cutoff )
    else : 
        return fixed_merged



def merge_ranges_liberal( prediction_ranges, scale ) : 

    merged_ranges = []
    # must manually loop so that we have control of the index
    ridx = -1
    while ridx < (len(prediction_ranges)-1) : 
        ridx += 1

        this_range =prediction_ranges[ridx]
        length = this_range.get_length()

        # merge if below threshold
        if length < scale*5 : 
            neighbor_idx = choose_merge_neighbor( prediction_ranges, ridx )

            # nothing do do, just add
            if neighbor_idx is None : 
                merged_ranges.append( this_range )
            else :
                if abs( neighbor_idx - ridx ) != 1 : 
                    raise RuntimeError( 'Neighbor index must be adjacent to index')

                if neighbor_idx < ridx : 
                    merged = get_previous_neighbor_merged_region( prediction_ranges,
                                                                  ridx )

                    merged_ranges[-1] = merged
                else : 
                    merged = get_next_neighbor_merged_region( prediction_ranges,
                                                                  ridx )

                    merged_ranges.append( merged )
                    ridx += 1
        else : 
            merged_ranges.append( this_range )

    fixed_merged = merge_adjacent_groups( merged_ranges ) 

    # do it again until there are no more changes
    if len( fixed_merged ) < len( prediction_ranges ) : 
        return merge_ranges_liberal( fixed_merged, scale )
    else : 
        return fixed_merged


def choose_merge_neighbor( ranges, ridx ) :

    sizes = get_neighbor_sizes( ranges, ridx )

    # stop if both are small
    pass_size_threshold = list(filter( lambda x : x > 60000, sizes ))

    if len(pass_size_threshold) == 0 :
        return None
    else : 
        comb_idx = sizes.index(max( sizes ) )
        if comb_idx == 0 : 
            return ridx - 1
        else : 
            return ridx + 1

def get_neighbor_sizes( regions, idx ) : 

    check_idx = get_neighbor_indices_check_edges( regions, idx )
    return [ regions[i].get_length() if i is not None else 0 for i in check_idx  ]

def get_neighbor_indices_check_edges( regions, idx) : 

    if idx == 0 : 
        check_idx = [None, idx+1]
    elif idx == ( len(regions) - 1 ) : 
        check_idx = [idx-1, None]
    else : 
        check_idx = [ idx-1, idx+1 ]

    return check_idx

def get_previous_neighbor_merged_region( regions, this_idx) :

    other_idx = this_idx - 1

    prev_reg = regions[other_idx]
    this_reg = regions[this_idx]

    return audio_region( prev_reg.get_start(), 
                         this_reg.get_stop(), 
                         prev_reg.get_value())
    
def get_next_neighbor_merged_region( regions, this_idx) :

    other_idx = this_idx + 1

    next_reg = regions[other_idx]
    this_reg = regions[this_idx]

    return audio_region( this_reg.get_start(), 
                         next_reg.get_stop(), 
                         next_reg.get_value())


def merge_adjacent_groups( merged_ranges ) : 

    # now we have to merge samples with the same color again
    # we don't care about the order, so just look forward
    fixed_merged = [merged_ranges[0]]

    for idx, this_region in enumerate(merged_ranges[1:]) : 

        # since we loop starting at 1: idx takes the prevous sample
        prev_region = merged_ranges[idx]

        # need to merge, modify the previous value
        if this_region.get_value() == prev_region.get_value() : 
            fixed_merged[-1] = audio_region( fixed_merged[-1].get_start(), 
                                             this_region.get_stop(), 
                                             this_region.get_value() )
        else : 
            fixed_merged.append( this_region )

    return fixed_merged


def get_unmerged_predictions( predicted_clusters, scale )  : 

    prediction_ranges = []
    for idx, pred in enumerate( predicted_clusters ) : 

        start = idx * scale
        end = (idx+1) * scale

        prediction_ranges.append( audio_region( start, end, pred ) )

    return prediction_ranges

def get_merged_predictions_simple( predicted_clusters, scale ) : 

    prediction_start = None
    prediction_val = None
    prediction_counts = {}
    prediction_ranges = []
    for idx, pred in enumerate( predicted_clusters ) : 

        if prediction_start is None : 
            prediction_start = idx * scale
            prediction_val = pred

        else : 
            # start a new range
            if pred != prediction_val : 
                prediction_ranges.append( audio_region( prediction_start, 
                                                        (idx)*scale, 
                                                        prediction_val ) 
                                        )
                prediction_start = idx*scale
                prediction_val = pred

    prediction_ranges.append( audio_region( prediction_start, 
                                            len(predicted_clusters)*scale, 
                                            prediction_val ) 
                            )

    return prediction_ranges

def label_regions_of_interest( simple_ranges, complex_ranges ) : 
    
    valid_simple = list(filter( lambda x : (not x.has_value('silence') and x.get_length() >= 60000 ), simple_ranges ))
    valid_complex = list(filter( lambda x : not x.has_value('silence'), complex_ranges ))

    rois = []

    for srange in valid_simple  : 
        overlap_value = None
        for crange in valid_complex: 

            all_olaps = []
            all_olaps.append(srange.get_begin_overlap_fraction( crange))
            all_olaps.append(srange.get_end_overlap_fraction( crange))
            all_olaps.append(srange.get_full_overlap_fraction(  crange ))

            if len(list(filter( lambda x : x > 0, all_olaps))) > 1 : 
                raise RuntimeError('Received multiple non-zero overlaps! ' + \
                    'this should not happen!')

            overlap = sum(all_olaps)

            if overlap > 0.8 : 
                overlap_value = crange.get_value()
                break

        labels = ['noise']
        if overlap_value is not None : 
            labels.append( overlap_value )

        rois.append( audio_region( srange.get_start(), srange.get_stop(), labels ) )

    return rois


def add_mapping_to_model( model, mapping ) :

    if not hasattr( model, 'mapping') : 
        if mapping is not None : 
            print ('Add mapping data to model!')
            map_data = utils.load_json( mapping )
            mod_data = {}
            for key, val in map_data.items() : 
                mod_data[int(key)] = val
            model.mapping = mod_data

if __name__ == '__main__' : 
    main( parse_args() )
