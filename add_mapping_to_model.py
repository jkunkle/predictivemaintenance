import argparse
import os

import utils

def parse_args() : 

    parser = argparse.ArgumentParser()

    parser.add_argument( '--modelFile', dest='modelFile', required=True, help='model file' )
    parser.add_argument( '--mappingFile', dest='mappingFile', required=True, help='mapping file' )

    return parser.parse_args()

def main( args ) : 

    run (**vars( args ) ) 

def run( modelFile, mappingFile ) : 

    model = utils.get_model( modelFile )

    mapping = utils.load_json( mappingFile )

    # sanity checks
    valid_entries = ['noise', 'silence', 'pump', 'grind', 'motor']

    diffs = set(valid_entries) - set( mapping.values() )
    
    for d in diffs : 
        print ('WARNING entry %s is not present in mapping!' %d )

    mod_mapping = {}
    for key, val in mapping.items() : 
        mod_mapping[int(key)] = val

    model.mapping = mod_mapping

    tmp_name = modelFile + '.tmp'
    utils.write_model( model,  tmp_name )

    os.system( 'mv %s %s' %( tmp_name, modelFile ) )
    

if __name__ == '__main__' : 
    main( parse_args() )
