import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np
import sys
import os
import pickle
from sklearn.cluster import KMeans

import make_cluster_model
import classify_waveforms

import argparse

_DATA_FORMAT = 'UInt8'
_ZERO_OFFSET = 128
_START_TRIM = 4000
_AVERAGE_WINDOW = 4000
_AVERAGE_WINDOW_SPECTRO = 4
_NOISE_THRESHOLD = 100

def parse_args() : 

    parser = argparse.ArgumentParser()
    
    parser.add_argument( '--modelFile', dest='modelFile', default=None, help='Path to model file' )
    parser.add_argument( '--audioFile', dest='audioFile', default=None, help='Path to audio file' )
    parser.add_argument( '--audioDir', dest='audioDir', default=None, help='Path to directory containing audio files' )

    return parser.parse_args()


def main( args ) : 

    run( **vars( args ) )


def run( modelFile, audioFile, audioDir ) : 

    mofile = open( modelFile, 'rb')

    kmeans_model = pickle.load( mofile )

    mofile.close()

    input_files = []
    if audioDir is not None : 
        for fname in os.listdir( audioDir ) : 
            if fname.count('.wav') : 
                input_files.append( '%s/%s' %( audioDir, fname ) )

    if audioFile is not None : 
        input_files.append( audioFile )

    for fname in input_files : 
        try : 
            plot_samples_with_categories( fname, kmeans_model )
        except ValueError as e : 
            print (e)
            continue



def plot_samples_with_categories( fname, model ) : 

    sample_rate, samples = make_cluster_model.read_file( fname )

    coarse_spectro, samples_per_spectro= make_cluster_model.make_coarse_spectrogram( samples, sample_rate, target_fps=model.FPS )


    #coarse_spectro = coarse_spectro.transpose()
    spectro_length = coarse_spectro.shape[0]

    predicted_clusters = model.predict( coarse_spectro )

    prediction_ranges = classify_waveforms.get_merged_predictions_simple( predicted_clusters, samples_per_spectro)
    #prediction_ranges = classify_waveforms.get_unmerged_predictions( predicted_clusters, samples_per_spectro)

    prediction_ranges = classify_waveforms.convert_to_group_names( prediction_ranges, model.mapping )
    plot_samples_with_ranges( samples, prediction_ranges )

    cutoff = 64//(196//model.FPS)
    final_ranges = classify_waveforms.merge_ranges_conservative( prediction_ranges, samples_per_spectro, cutoff )

    plot_samples_with_ranges( samples, final_ranges )

def plot_samples_with_ranges( samples, ranges ) :

    if not isinstance( ranges, list ) : 
        ranges = [ranges]
    if not isinstance( ranges[0], list ) :
        ranges = [ranges]

    colors = ['r', 'b', 'g', 'm', 'y', 'c']
    #colors = ['#8B3E6E', '#4DBAC3', '#C6C474', '#682D55', '#E3E1CD', '#333333']
    #colors = ['#8B3E6E', '#4DBAC3', '#C6C474', '#333333', '#682D55', '#E3E1CD']

    color_mapping = {'grind' : 'r', 'silence' : 'b', 'pump' : 'g', 'motor' : 'c', 'clean' : 'm', 'noise' : 'y'}

    #color_mapping = {}
    #all_pred = list( set( [r[2] for r in ranges] ) )
    #all_pred.sort()
    #print (all_pred)
    #for pidx, p in enumerate(all_pred) : 
    #    color_mapping[p] = colors[pidx]

    #print (color_mapping)

    fig, axes = plt.subplots(len(ranges), 1, figsize= (30,20) )


    if not isinstance( axes, np.ndarray) : 
        axes = [axes]

    for  rang, ax in zip(ranges, axes) : 

        ax.plot(samples, color='b')
        ylims = ax.get_ylim()

        for r in rang : 
            rect = Rectangle( (r.get_start(), ylims[0]), r.get_length(), ylims[1]-ylims[0], color=color_mapping[r.get_value()], alpha=0.5)
            ax.add_patch( rect )

    plt.show()
    fig.clear()
    plt.close(fig)



if __name__ == '__main__' : 
    main( parse_args() )


