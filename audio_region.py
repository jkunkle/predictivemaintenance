import datetime
import math

class audio_region : 

    def __init__( self, start, stop, val, metadata=None ) : 

        if stop == start :
            raise ValueError( 'Start and stop have same values giving zero length!' )
        if stop < start :
            raise ValueError( 'Stop is before Start giving negative length!' )

        if not type(start) == type(stop) : 
            raise ValueError( 'Stop and start must be of the same type' )

        if isinstance( start, datetime.datetime ) : 
            self._type = 'absolute'
        else : 
            self._type = 'relative'

        if metadata is None : 
            self._metadata = {}
        else : 
            self._metadata = metadata

        # set values
        self.set_start( start )
        self.set_stop( stop )
        self.set_value( val )
    

    def __eq__( self, other  ) : 
        return (     self.get_start() == other.get_start() \
                 and self.get_stop()  == other.get_stop() \
                 and self.get_value() == other.get_value() )

    def __str__(self) : 
        start = self.get_start()
        stop = self.get_stop()
        length = self.get_length()
        if isinstance( length, datetime.timedelta ) : 
            length = length.total_seconds()
        val = self.get_value()
        _str = f'Start = {start}, Stop = {stop}, length = {length}, Value = {val}'

        return _str

    def copy( self ) : 
        return audio_region( self.get_start(), self.get_stop(), self.get_value() )

    def set_start(self, val) : 
        self._start = val
    def set_stop(self, val) : 
        self._stop = val
    def set_value(self, val) : 
        self._value = val
    def set_time(self, val ) : 
        self._timestamp = val
    def add_metadata( self, key, val ) : 
        self._metadata[key] = val

    def get_metadata( self, key=None ) : 
        if key is None : 
            return self._metadata
        else : 
            return self._metadata[key]

    def get_start( self ) : 
        return self._start
    def get_stop( self ) : 
        return self._stop
    def get_value( self ) : 
        return self._value
    def get_type(self) : 
        return self._type

    def has_value( self, val) : 
        return self._value == val

    def get_length( self ) : 
        return ( self.get_stop() - self.get_start()) 

    def to_dict( self ) : 
        return { 'start' : self.get_start(), 
                 'stop'  : self.get_stop(), 
                 'value' : self.get_value(),
                 'type'  : self.get_type(),
                 'metadata' : self.get_metadata() }

    def from_dict(self, data) : 

        if 'start' in data : 
            self.set_start( data['start'] )
        if 'stop' in data : 
            self.set_stop( data['stop'] )
        if 'value' in data : 
            self.set_value( data['value'] )
        if 'type' in data : 
            self._type = data['type']
        if 'metadata' in data : 
            self.set_metadata( data['metadata'] )


    def convert_to_absolute_time( self, start_time, sample_rate ) : 

        if self._type == 'absolute'  : 
            print ('Already using absolute time!')
            return
        
        self._type = 'absolute'

        self.add_metadata('sample_start',  math.floor( self.get_start() ))
        self.add_metadata('sample_stop',  math.ceil( self.get_stop() ))

        stop_time =  start_time + datetime.timedelta( seconds=self.get_length()/sample_rate)
        self.set_start( start_time )
        self.set_stop( stop_time )





    def get_begin_overlap_fraction( self, other ) : 
        """ 
        get overlaping fraction when other partially overlaps with begining of range 

        Args : 
            param1 : other -- audio_region
        Returns : 
            float : fraction or 0 if no overlap found
        """

        if     other.get_stop()  > self.get_start()  \
           and other.get_stop()  < self.get_stop() \
           and other.get_start() < self.get_start()  : 
            return (other.get_stop() - self.get_start())/self.get_length()
        else :
            return 0

    def get_end_overlap_fraction( self, other ) : 
        """ 
        get overlaping fraction when other partially overlaps with end of range 

        Args : 
            param1 : other -- audio_region
        Returns : 
            float : fraction or 0 if no overlap found
        """

        if     other.get_start()  > self.get_start()  \
           and other.get_start()  < self.get_stop() \
           and other.get_stop() > self.get_stop ()  : 
            return (self.get_stop() - other.get_start())/self.get_length()
        else :
            return 0

    def get_full_overlap_fraction( self, other ) : 
        """ 
        get overlaping fraction when other fully overlaps with range 

        Args : 
            param1 : other -- audio_region
        Returns : 
            float : fraction or 0 if no overlap found
        """

        if     other.get_start() >= self.get_start()  \
           and other.get_stop() <= self.get_stop ()  : 
            return (other.get_stop() - other.get_start())/self.get_length()
        else :
            return 0




