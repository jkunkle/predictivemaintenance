import unittest
from audio_region import audio_region
import classify_waveforms as classify
import make_cluster_model as mcm
import numpy as np

_TEST_START = 10
_TEST_STOP = 78
_TEST_VAL = 1

_TEST_LEN = _TEST_STOP - _TEST_START

_TEST_REGIONS = [
                 audio_region(_TEST_START, 
                              _TEST_STOP, 
                              _TEST_VAL
                              ),
                 audio_region(_TEST_START+_TEST_LEN, 
                              _TEST_STOP+_TEST_LEN, 
                              _TEST_VAL
                              ),
                 audio_region(_TEST_START+2*_TEST_LEN, 
                              _TEST_STOP+2*_TEST_LEN, 
                              _TEST_VAL),
                 ]

class TestMakeClusterModel( unittest.TestCase ) : 

    def test_merge_samples( self ) : 

        def _get_comparision( X_DIM, Y_DIM, WINDOW, AXIS, OFFSET ) : 

            if AXIS == 0 : 
                return _get_comparison_x( X_DIM, Y_DIM, WINDOW, OFFSET )
            else : 
                return _get_comparison_y( X_DIM, Y_DIM, WINDOW, OFFSET)

        def _get_comparison_y( X_DIM, Y_DIM, WINDOW, OFFSET ) : 

            compare = []

            n_full_merge = (Y_DIM-OFFSET)//WINDOW
            residual_merge = (Y_DIM-OFFSET)%WINDOW

            for i in range(0, X_DIM ) :
                merge_vals = list(range( Y_DIM*i, Y_DIM*(i+1) ))

                merged = []
                if OFFSET : 
                    merged += merge_vals[0:OFFSET]
                for j in range( 0, n_full_merge*WINDOW, WINDOW ) : 
                    merged.append( sum( merge_vals[j+OFFSET:j+WINDOW+OFFSET] ) )
                if residual_merge : 
                    merged.append( sum( merge_vals[Y_DIM-residual_merge:]) )

                compare.append( merged )

            return compare
                    



        def _get_comparison_x( X_DIM, Y_DIM, WINDOW, OFFSET ) :


            n_full_merge = (X_DIM-OFFSET)//WINDOW
            residual_merge = (X_DIM-OFFSET) % WINDOW
            compare = []

            if OFFSET : 
                for i in range( 0, OFFSET ) : 
                    compare.append(list( range( i*Y_DIM, (i+1)*Y_DIM) ) )

            for i in range( 0, n_full_merge ) : 
                starting_val = sum(list(range( WINDOW*i+OFFSET, WINDOW*(i+1)+OFFSET )))*Y_DIM

                this_val = list(range( starting_val, starting_val+(WINDOW*Y_DIM), WINDOW ))
                compare.append( this_val )

            if residual_merge : 
                start_idx = (X_DIM-residual_merge)
                start_val = sum( list( range( start_idx, 100, 1 ) ) )*10
                compare.append( list( range( start_val, start_val+residual_merge*Y_DIM, residual_merge ) ) )

            return compare
            
        X_DIM = 100
        Y_DIM = 10
        WINDOW = 5
        OFFSET = 0

        test_data = np.array( range( 0, 1000 ) ).reshape( X_DIM, Y_DIM )
        
        merged = mcm.merge_samples( test_data, WINDOW, 0,  OFFSET )
        compare = _get_comparision( X_DIM, Y_DIM, WINDOW, 0, OFFSET )
        self.assertEqual( compare, merged.tolist())

        WINDOW = 6
        merged = mcm.merge_samples( test_data, WINDOW, 0,  OFFSET )
        compare = _get_comparision( X_DIM, Y_DIM, WINDOW, 0, OFFSET )
        self.assertEqual( compare, merged.tolist())

        WINDOW = 30 
        merged = mcm.merge_samples( test_data, WINDOW, 0,  OFFSET )
        compare = _get_comparision( X_DIM, Y_DIM, WINDOW, 0, OFFSET )
        self.assertEqual( compare, merged.tolist())

        WINDOW = 3
        OFFSET = 1
        merged = mcm.merge_samples( test_data, WINDOW, 0,  OFFSET )
        compare = _get_comparision( X_DIM, Y_DIM, WINDOW, 0, OFFSET )
        self.assertEqual( compare, merged.tolist())

        WINDOW = 4
        OFFSET = 2
        merged = mcm.merge_samples( test_data, WINDOW, 0,  OFFSET )
        compare = _get_comparision( X_DIM, Y_DIM, WINDOW, 0, OFFSET )
        self.assertEqual( compare, merged.tolist())

        WINDOW=5
        OFFSET=0
        merged = mcm.merge_samples( test_data, WINDOW, 1,  OFFSET )
        compare = _get_comparision( X_DIM, Y_DIM, WINDOW, 1, OFFSET )
        self.assertEqual( compare, merged.tolist())

        WINDOW=4
        OFFSET=0
        merged = mcm.merge_samples( test_data, WINDOW, 1,  OFFSET )
        compare = _get_comparision( X_DIM, Y_DIM, WINDOW, 1, OFFSET )
        self.assertEqual( compare, merged.tolist())

        WINDOW=3
        OFFSET=1
        merged = mcm.merge_samples( test_data, WINDOW, 1,  OFFSET )
        compare = _get_comparision( X_DIM, Y_DIM, WINDOW, 1, OFFSET )
        self.assertEqual( compare, merged.tolist())

        WINDOW=2
        OFFSET=1
        merged = mcm.merge_samples( test_data, WINDOW, 1,  OFFSET )
        compare = _get_comparision( X_DIM, Y_DIM, WINDOW, 1, OFFSET )
        self.assertEqual( compare, merged.tolist())





class TestClassify( unittest.TestCase ) : 

    def test_get_neighbor_sizes(self ) :

        self.assertEqual( 
            classify.get_neighbor_sizes( _TEST_REGIONS, 0 ),
            [0, _TEST_REGIONS[1].get_length()] )

        self.assertEqual( 
            classify.get_neighbor_sizes( _TEST_REGIONS, 1 ),
            [_TEST_REGIONS[0].get_length(), _TEST_REGIONS[2].get_length()] )

        self.assertEqual( 
            classify.get_neighbor_sizes( _TEST_REGIONS, 2 ),
            [_TEST_REGIONS[2].get_length(), 0] )

    def test_get_neighbor_indices_check_edges( self ) : 

        self.assertEqual( classify.get_neighbor_indices_check_edges( 
                     _TEST_REGIONS, 0 ), [None, 1] )

        self.assertEqual( classify.get_neighbor_indices_check_edges( 
                     _TEST_REGIONS, 1 ), [0, 2] )

        self.assertEqual( classify.get_neighbor_indices_check_edges( 
                     _TEST_REGIONS, 2 ), [1, None] )

    def test_get_previous_neighbor_merged_region(  self ) : 

        merged = classify.get_previous_neighbor_merged_region( 
                 _TEST_REGIONS,
                 1 )

        self.assertEqual( merged, audio_region( _TEST_START, 
                                                _TEST_STOP+_TEST_LEN, 
                                                _TEST_VAL)
                   )

    def test_get_next_neighbor_merged_region(  self ) : 
        merged = classify.get_next_neighbor_merged_region( 
                 _TEST_REGIONS,
                 1 )

        self.assertEqual( merged, audio_region( _TEST_START+_TEST_LEN, 
                                                _TEST_STOP+2*_TEST_LEN, 
                                                _TEST_VAL)
                   )



class TestAudioRegion( unittest.TestCase ) : 

    def test_value_error( self ) : 

        self.assertRaises( ValueError, audio_region.__init__, audio_region,
                          _TEST_STOP, _TEST_START, _TEST_VAL ) 

    def test_eq( self ) : 

        region = audio_region( _TEST_START, _TEST_STOP, _TEST_VAL )
        
        other_eq = audio_region( _TEST_START, _TEST_STOP, _TEST_VAL )
        self.assertTrue( region == other_eq )

        other_neq = audio_region( _TEST_START+1, _TEST_STOP, _TEST_VAL )
        self.assertFalse( region == other_neq )

        other_neq = audio_region( _TEST_START, _TEST_STOP+1, _TEST_VAL )
        self.assertFalse( region == other_neq )

        other_neq = audio_region( _TEST_START, _TEST_STOP, _TEST_VAL+1 )
        self.assertFalse( region == other_neq )



    def test_get_start( self ) : 

        region = audio_region( _TEST_START, _TEST_STOP, _TEST_VAL )
        self.assertEqual( region.get_start(), _TEST_START )

    def test_get_stop( self ) : 

        region = audio_region( _TEST_START, _TEST_STOP, _TEST_VAL )
        self.assertEqual( region.get_stop(), _TEST_STOP )

    def test_get_value( self ) : 

        region = audio_region( _TEST_START, _TEST_STOP, _TEST_VAL )
        self.assertEqual( region.get_value(), _TEST_VAL )

    def test_get_length( self ) : 

        region = audio_region( _TEST_START, _TEST_STOP, _TEST_VAL )
        self.assertEqual( region.get_length(), _TEST_STOP - _TEST_START )

    def test_get_begin_overlap_fraction( self ) : 

        region = audio_region( _TEST_START, _TEST_STOP, _TEST_VAL )

        other_early_outside = audio_region( _TEST_START-10, _TEST_START-2, _TEST_VAL )
        self.assertEqual( region.get_begin_overlap_fraction( other_early_outside ), 0 )

        other_late_outside = audio_region( _TEST_STOP+10, _TEST_STOP+20, _TEST_VAL )
        self.assertEqual( region.get_begin_overlap_fraction( other_late_outside), 0 )

        other_inside = audio_region( _TEST_START+10, _TEST_STOP-2, _TEST_VAL )
        self.assertEqual( region.get_begin_overlap_fraction( other_inside), 0 )

        other_exact = audio_region( _TEST_START, _TEST_STOP, _TEST_VAL )
        self.assertEqual( region.get_begin_overlap_fraction( other_exact), 0 )

        other_ovlp_late = audio_region( _TEST_START + 5, _TEST_STOP+5, _TEST_VAL )
        self.assertEqual( region.get_begin_overlap_fraction( other_ovlp_late), 0 )
        
        other_start = _TEST_START-5
        other_stop = _TEST_STOP-5
        other_ovlp_early = audio_region( other_start, other_stop, _TEST_VAL )
        self.assertEqual( region.get_begin_overlap_fraction( other_ovlp_early ), (other_stop-region.get_start())/region.get_length() )

    def test_get_end_overlap_fraction( self ) : 

        region = audio_region( _TEST_START, _TEST_STOP, _TEST_VAL )

        other_early_outside = audio_region( _TEST_START-10, _TEST_START-2, _TEST_VAL )
        self.assertEqual( region.get_end_overlap_fraction( other_early_outside ), 0 )

        other_late_outside = audio_region( _TEST_STOP+10, _TEST_STOP+20, _TEST_VAL )
        self.assertEqual( region.get_end_overlap_fraction( other_late_outside), 0 )

        other_inside = audio_region( _TEST_START+10, _TEST_STOP-2, _TEST_VAL )
        self.assertEqual( region.get_end_overlap_fraction( other_inside), 0 )

        other_exact = audio_region( _TEST_START, _TEST_STOP, _TEST_VAL )
        self.assertEqual( region.get_end_overlap_fraction( other_exact), 0 )

        other_ovlp_early = audio_region( _TEST_START-5, _TEST_STOP-5, _TEST_VAL )
        self.assertEqual( region.get_end_overlap_fraction( other_ovlp_early), 0 )

        other_start = _TEST_START + 5
        other_stop  = _TEST_STOP  + 5
        other_ovlp_late = audio_region( other_start,other_stop , _TEST_VAL )
        self.assertEqual( region.get_end_overlap_fraction( other_ovlp_late ), (region.get_stop() - other_start)/region.get_length() )

    def test_get_full_overlap_fraction( self ) : 

        region = audio_region( _TEST_START, _TEST_STOP, _TEST_VAL )

        other_early_outside = audio_region( _TEST_START-10, _TEST_START-2, _TEST_VAL )
        self.assertEqual( region.get_full_overlap_fraction( other_early_outside ), 0 )

        other_late_outside = audio_region( _TEST_STOP+10, _TEST_STOP+20, _TEST_VAL )
        self.assertEqual( region.get_full_overlap_fraction( other_late_outside), 0 )

        other_ovlp_early = audio_region( _TEST_START-5, _TEST_STOP-5, _TEST_VAL )
        self.assertEqual( region.get_full_overlap_fraction( other_ovlp_early), 0 )

        other_ovlp_late = audio_region( _TEST_START + 5, _TEST_STOP+5, _TEST_VAL )
        self.assertEqual( region.get_full_overlap_fraction( other_ovlp_late), 0 )

        other_exact = audio_region( _TEST_START, _TEST_STOP, _TEST_VAL )
        self.assertEqual( region.get_full_overlap_fraction( other_exact), 1 )

        other_start = _TEST_START+10
        other_stop = _TEST_STOP-2
        other_inside = audio_region( other_start, other_stop,  _TEST_VAL )
        self.assertEqual( region.get_full_overlap_fraction( other_inside), ( other_stop-other_start)/region.get_length() )




if __name__ == '__main__' :
    unittest.main()

