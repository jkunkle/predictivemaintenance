import matplotlib.pyplot as plt
import numpy as np
import sys
import random
import math
import json
import os
import pickle
from scipy.io import wavfile
from scipy import signal
from scipy import sparse
from sklearn.cluster import KMeans
from sklearn.cluster import DBSCAN
from sklearn.cluster import AgglomerativeClustering

import argparse

_DATA_FORMAT = 'UInt8'
_ZERO_OFFSET = 128
_START_TRIM = 4000
_AVERAGE_WINDOW = 4000
_AVERAGE_WINDOW_SPECTRO = 4
_NOISE_THRESHOLD = 100

def parse_args() : 

    parser = argparse.ArgumentParser()
    
    parser.add_argument( '--inputFile', dest='inputFile', default=None, help='Path to input file' )
    parser.add_argument( '--inputDir', dest='inputDir', default=None, help='Path to directory containing input files' )
    parser.add_argument( '--spectrumData', dest='spectrumData', default=None, help='Path to json file with waveform data' )
    parser.add_argument( '--clusterResultFile', dest='clusterResultsFile', default='cluster_results.pickle', help='Path to pickle file with cluster parameters data' )
    parser.add_argument( '--clusterDataFromJson', dest='clusterDataFromJson', default=False, action='store_true', help='Run clustering from json file data, --spectrumData is required' )
    parser.add_argument( '--targetFPS', dest='target_fps', default=24, type=int,  help='Number of FPS to use for clustering' )
    parser.add_argument( '--nClusters', dest='nClusters', default=5, type=int,  help='Number of clusters to use for clustering' )


    return parser.parse_args()


def main( args ) : 

    run( **vars( args ) )

def merge_samples( data, comb_window, axis=0, offset=0 ) : 

    other_axis = abs( int(axis - 1) )

    new_length = math.ceil((data.shape[axis]-offset)/comb_window ) + offset

    data_mod = data.tolist()
    spectro_merge = []

    #if axis == 0 :
    #    spectro_merge = np.zeros( (new_length, data.shape[other_axis] ))
    #else :
    #    spectro_merge = np.zeros( (data.shape[other_axis], new_length))

    for iother in range( 0,  data.shape[other_axis]  )  :

        this_data = [0]*new_length
        for ival in range( 0, data.shape[axis] ) :
            if ival <= (offset - 1) :
                inew_window = ival
            else :
                inew_window = ((ival-offset) // comb_window) + offset

            if axis == 0 :
                this_data[inew_window] += data_mod[ival][iother]
                #spectro_merge[inew_window][iother] += data_mod[ival][iother]
            else :
                this_data[inew_window] += data_mod[iother][ival]
                #spectro_merge[iother][inew_window] += data_mod[iother][ival]

        spectro_merge.append( this_data )

    spectro_merge = np.array( spectro_merge )
    if axis == 0 : 
        spectro_merge = spectro_merge.transpose()
    return spectro_merge


def run( inputFile=None, inputDir=None, spectrumData=None, clusterDataFromJson=False, clusterResultsFile=None, target_fps=24, nClusters=5 ) : 

    if clusterDataFromJson : 
        assert spectrumData is not None, 'spectrumData must be provided!'

        ofile = open( spectrumData, 'r' )


        all_data = json.load( ofile )

        ofile.close()


    else : 

        if spectrumData is not None : 
            if os.path.isfile( spectrumData ) :  
                res = input( 'Data file, %s, already exists! Overwrite? y/[n]' %spectrumData )
                if res.lower() != 'y' : 
                    print ('Stopping')
                    sys.exit(-1)

        all_data = process_wav_files( inputFile, inputDir, target_fps)

        if spectrumData is not None :
            ofile = open( spectrumData, 'w')
            json.dump( all_data, ofile )
            ofile.close()

    model = run_clustering( all_data, clusterResultsFile, nClusters )

    model.FPS = target_fps

    ofile = open( clusterResultsFile, 'wb') 

    pickle.dump( model, ofile )

    ofile.close()


def run_clustering( input_data, resultsFile, nClusters ) : 

    comb_data = None
    print ('Gathering Data')
    for fkey, data in input_data.items() : 
        #if comb_data is None : 
        #    comb_data = np.array(data['data'])
        #else : 
        #    comb_data = np.append( comb_data, np.array(data['data']), axis=0 )

        if comb_data is None : 
            comb_data = data['data']
        else : 
            comb_data += data['data']

    print ('Fit model')
    kmean = KMeans( n_clusters=nClusters ).fit( comb_data )
    #kmean = DBSCAN( 0.8, 1000 ).fit( comb_data )
    #kmean = AgglomerativeClustering( n_clusters=5 ).fit( comb_data )

    return kmean


def process_wav_files( inputFile=None, inputDir=None, target_fps=24  ) :

    input_files = []

    if inputDir is not None : 

        for fname in os.listdir( inputDir ) : 
            if fname.count('.wav' ) : 
                input_files.append( '%s/%s' %( inputDir, fname ) )

    if inputFile is not None : 
        input_files.append( inputFile )

    assert input_files, 'No input files found!'

    all_data = {}
    total_files = len( input_files) 
    for fidx, f in enumerate(input_files) : 

        print ('On file %d of %d' %( fidx, total_files ) )

        try : 
            data, scale =  get_coarse_spectrogram( f, target_fps )
            all_data[f] = {'data' : data.tolist(), 'scale' : scale }
        except ValueError as e : 
            print( e)
            continue


    return all_data

def read_file( input_file ) : 

    return wavfile.read(input_file,'r')

def get_coarse_spectrogram( input_file, target_fps=24 ) : 

    sample_rate, samples = read_file(input_file)
    print ('Sample rate = ', sample_rate )

    return make_coarse_spectrogram( samples, sample_rate, target_fps )

def make_coarse_spectrogram( samples, sample_rate, target_fps=24 ) :

    if target_fps > 196 : 
        print ('Maximum sectrogram sample rate is 196 FPS, defaulting to this')
        target_fps = 196

    length = samples.shape[0]/sample_rate
    print ('Original length = ',  length)

    frequencies, times, spectrogram = signal.spectrogram(samples, sample_rate, nperseg=256)
    print ('FREQ shape = ', frequencies.shape )
    print ('times shape = ', times.shape )
    print ('spectro shape = ', spectrogram.shape )
    print ('Spectro samples per sec = ', times.shape[0]/length ) 
    sum_window_freq = 4
    # based on the FFT above, a summing window of target_fps/3 is needed
    sum_window_samp = 196//target_fps

    #window_spectro = np.ones( (_AVERAGE_WINDOW_SPECTRO,))/(_AVERAGE_WINDOW_SPECTRO)
    #spectro_avg = np.apply_along_axis(lambda m: np.convolve(m, window_spectro, mode='valid'), axis=1, arr=spectrogram)

    #merged_samples = merge_samples(  spectro_avg, sum_window_freq, axis=0 )
    merged_samples = merge_samples(  spectrogram, sum_window_freq, axis=0, offset=1 )
    spectro_comb = merge_samples(  merged_samples, sum_window_samp, axis=1 )

    mod_spectro = np.log10( spectro_comb ).transpose()

    n_samples_per_spectro = samples.shape[0]/mod_spectro.shape[0]

    return (np.array( mod_spectro) , n_samples_per_spectro )

    #out_spectro = []

    #for idx in range(0, mod_spectro.shape[0]) : 

    #    this_spectro = mod_spectro[idx].tolist()
    #   
    #    advance_idx = idx + 4
    #    if advance_idx >= mod_spectro.shape[0] : 
    #        this_spectro.append( 1.0 )
    #    else :
    #        my_sum = np.sum( mod_spectro[idx] )
    #        advance_sum = np.sum( mod_spectro[advance_idx] )
    #        this_spectro.append( my_sum/advance_sum )

    #    out_spectro.append( this_spectro )

    ##return (np.log10(spectro_comb), sample_rate, sample_length )
    #return (np.array( out_spectro) , sample_rate, sample_length )

#def process_other( inputFile ) :
#
#    sample_rate, samples = wavfile.read(inputFile,'r')
#
#    signal_fabs = np.fabs( samples )
#    #time_axis=np.linspace(0, len(signal)/fs, num=len(signal))
#    #time_axis=np.linspace(0, len(signal), num=len(signal))
#
#    window = np.ones((_AVERAGE_WINDOW,))/(_AVERAGE_WINDOW)
#    running_avg = np.convolve(signal_fabs, window, mode='same')
#    #running_avg = np.convolve(signal_fabs, window)
#
#
#    frequencies, times, spectrogram = signal.spectrogram(samples, sample_rate, nperseg=256)
#
#    #window_spectro = np.ones( (_AVERAGE_WINDOW_SPECTRO,))/(_AVERAGE_WINDOW_SPECTRO)
#    #spectro_avg = np.apply_along_axis(lambda m: np.convolve(m, window_spectro, mode='valid'), axis=1, arr=spectrogram)
#
#    comb_window = 8
#    new_freq_length = math.ceil(spectro_avg.shape[0]/comb_window )
#    print (new_freq_length)
#
#    #merged_samples = merge_samples(  spectro_avg, comb_window, axis=0 )
#    merged_samples = merge_samples(  spectrogram, comb_window, axis=0 )
#    spectro_comb = merge_samples(  merged_samples, comb_window*4, axis=1 )
#
#
#    #print (spectro_comb.shape)
#
#    #print (spectrogram.shape)
#    #print (spectro_avg.shape)
#    #fig, ax = plt.subplots()
#    ###plt.imshow(spectro_comb)
#    #plt.imshow(np.log10(spectro_comb))
#    #ax.set_aspect( 10 )
#    #plt.ylabel('Frequency [Hz]')
#    #plt.xlabel('Time [sec]')
#    #plt.show()
#
#    segment_idx = []
#    start_idx = None
#    for idx, val in enumerate(running_avg) : 
#        if val > _NOISE_THRESHOLD and start_idx is None : 
#            start_idx = max(idx , 0 )
#
#        if val < _NOISE_THRESHOLD and start_idx is not None : 
#            segment_idx.append( ( start_idx, idx - _AVERAGE_WINDOW//2 ) )
#            start_idx = None
#
#    if start_idx is not None : 
#        segment_idx.append( (start_idx, len( running_avg) - 1 ) )
#
#    fig, ax = plt.subplots()
#    plt.title('Making Coffee')
#    #plt.plot(time_axis,running_avg)
#    plt.plot(samples, color='b')
#    plt.plot(running_avg, color='k')
#
#    for start, stop in segment_idx : 
#        ax.axvline( start, color='g' )
#        ax.axvline( stop, color='r' )
#
#    plt.show()


if __name__ == '__main__' : 
    main( parse_args() )


