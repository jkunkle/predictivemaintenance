import argparse
import pandas
import json

def parse_options() : 

    parser = argparse.ArgumentParser()

    parser.add_argument( '--seq_file', dest='seq_file', required=True, help='path to sequences file')
    parser.add_argument( '--seq_type', dest='seq_type', default='grind', help='sequence type' )
    parser.add_argument( '--audio_type', dest='audio_type', default='motor', help='sequence type' )

    return parser.parse_args()

def main(options) : 

    make_model_templates(**vars(options))

def make_model_templates(seq_file, seq_type, audio_type) :

    df = pandas.DataFrame.from_csv( seq_file )

    sel_seq = get_selected_sequences( df, seq_type, audio_type ) 

    print (list(sel_seq['source']))

    get_seq_spectrograms( sel_seq )

    make_templates( sel_seq ) 

def get_selected_sequences( sequences, seq_type, audio_type ) : 

    return sequences[ ( (sequences['SequenceLabel'] == seq_type) & (sequences['AssignedType'] == audio_type)) ]




if __name__ == '__main__' : 
    main( parse_options() )
