import json
import pickle

def get_precise_seconds( obj ) : 

    return obj.seconds + obj.microseconds/1000000.

def get_model( fname ) :

    ofile = open( fname, 'rb' )

    model = pickle.load( ofile )

    ofile.close()

    return model

def write_model( model, fname ) : 

    ofile = open( fname, 'wb' )

    pickle.dump( model, ofile )

    ofile.close()

def load_json( fname ) : 

    ofile = open( fname ) 

    data = json.load( ofile )

    ofile.close()

    return data
